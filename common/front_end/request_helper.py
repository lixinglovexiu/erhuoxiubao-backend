import json

from common.exceptions import ParamsError


def parse_request_body_to_json(request):
    if not request.body:
        raise ParamsError(msg='body is missing', displaymsg='请求数据错误')

    try:
        body = json.loads(request.body)
    except Exception as e:
        raise ParamsError(msg='request body is not json', displaymsg='请求数据格式错误')

    return body


