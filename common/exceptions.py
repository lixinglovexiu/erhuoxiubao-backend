import functools
import logging

from common.response import error_response


def exception_wrapper(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except ALException as e:
            return error_response(code=e.code, msg=e.msg, displaymsg=e.displaymsg)
        except Exception as e:
            logging.exception(e)
            error_message = str(e)
            return error_response(msg=error_message, displaymsg=error_message)

    return wrapper


class ALException(Exception):
    code = 500
    msg = '内部错误'
    displaymsg = '内部错误'

    def __init__(self, code=None, msg=None, displaymsg=None):
        Exception.__init__(self)
        self.code = code or self.code
        self.msg = msg or displaymsg or self.msg or self.displaymsg
        self.displaymsg = displaymsg or msg or self.displaymsg or self.msg

    def to_dict(self):
        return {
            'result': {
                'success': False,
                'code': self.code,
                'msg': self.msg,
                'displaymsg': self.displaymsg,
            },
            'data': {}
        }


class BadRequest(ALException):
    code = 400
    msg = '请求无效'
    displaymsg = '请求无效'


class NotAuthorized(ALException):
    code = 401
    msg = '请重新登录'
    displaymsg = '请重新登录'


class NotFound(ALException):
    code = 404
    msg = '未找到'
    displaymsg = '未找到'


class MethodNotAllowed(ALException):
    code = 405
    msg = '请求方法不支持'
    displaymsg = '请求方法不支持'


class InternalError(ALException):
    code = 500
    msg = '内部错误'
    displaymsg = '内部错误'


class ParamsError(ALException):
    code = 1001
    msg = '参数错误'
    displaymsg = '参数错误'


class InvalidCaptcha(ALException):
    code = 1002
    msg = '验证码错误'
    displaymsg = '验证码错误'


class InvalidLoginType(ALException):
    code = 1003
    msg = '登录类型错误'
    displaymsg = '登录类型错误'


class StateError(ALException):
    code = 1004
    msg = '当前状态不允许该操作'
    displaymsg = '当前状态不允许该操作'


class AuditorError(ALException):
    code = 1005
    msg = '当前操作人不允许该操作'
    displaymsg = '当前操作人不允许该操作'