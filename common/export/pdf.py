import requests, pdfkit
from django.http import HttpResponse
from django.utils.encoding import escape_uri_path
from common.utils import file_helper



def test(request):
    url = 'https://yhj-ax-1256661917.cos.ap-beijing.myqcloud.com' \
          '/ax-dev/pdf/1525922419066-f175640e-952f-65d2-d381-0e23e8245f16.pdf'
    file_name = '1.pdf'
    return download_file(url, file_name)


def download_file(url, file_name):
    r = requests.get(url, stream=True)
    f = open(file_name, 'wb')
    for chunk in r.iter_content(chunk_size=512):
        if chunk:
            f.write(chunk)
    f.close()

    f_read = open(file_name, 'rb')
    response = HttpResponse(f_read, content_type='application/pdf')
    response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(
        escape_uri_path(file_name))
    f_read.close()
    # 删除存在本地的临时文件
    file_helper.remove_tmp_file(file_name)

    return response


def html2pdf(url, out_path):
    options = {
        'page-size': 'Letter',
        'margin-top': '0.75in',
        'margin-right': '0.75in',
        'margin-bottom': '0.75in',
        'margin-left': '0.75in',
        'encoding': "UTF-8",
        'no-outline': None
    }

    pdfkit.from_url(url, out_path, options=options)


if __name__ == '__main__':
    url = 'https://www.baidu.com'
    # out_path = '/Users/lixing/Desktop/out.pdf'
    out_path = '/opt/out.pdf'
    html2pdf(url, out_path)
