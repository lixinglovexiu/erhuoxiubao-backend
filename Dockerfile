FROM python:3
MAINTAINER lixing, Inc. Backend Awesome Boy

EXPOSE 8000

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install -i https://mirrors.aliyun.com/pypi/simple --no-cache-dir -r requirements.txt

COPY . /usr/src/app

CMD gunicorn -b :8000 -w 4 -k gevent erhuoxiubao.wsgi:application
