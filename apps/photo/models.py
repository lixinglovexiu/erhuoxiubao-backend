from django.db import models

from apps.base_model import BaseModel


class Photo(BaseModel):
    url = models.CharField(verbose_name='url', max_length=1000)
    desc = models.TextField(verbose_name='描述')

    class Meta:
        db_table = 'photo'
        ordering = ['-id']

    def get_json(self):
        return {
            'id': self.id,
            'url': self.url,
            'desc': self.desc,
        }


class PhotoWall(BaseModel):
    name = models.CharField(verbose_name='名称', max_length=1000)
    desc = models.TextField(verbose_name='描述', null=True)
    type = models.IntegerField(verbose_name='类型', default=0)
    photo_ids = models.TextField(verbose_name='照片ids')

    class Meta:
        db_table = 'photo_wall'
        ordering = ['-id']

    def get_json(self):
        images = self.get_images()
        return {
            'id': self.id,
            'name': self.name,
            'type': self.type,
            'desc': self.desc,
            'images': images,
        }

    def get_images(self):
        photo_id_array = eval(self.photo_ids)
        images = []
        for photo_id in photo_id_array:
            photo = Photo.get_by_id(photo_id)
            images.append(photo.get_json())
        return images
