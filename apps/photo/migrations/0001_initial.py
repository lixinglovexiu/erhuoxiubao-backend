# Generated by Django 2.0.1 on 2018-10-18 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('is_deleted', models.BigIntegerField(default=0, verbose_name='删除标记')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('url', models.CharField(max_length=1000, verbose_name='url')),
                ('desc', models.TextField(verbose_name='描述')),
            ],
            options={
                'db_table': 'photo',
                'ordering': ['-id'],
            },
        ),
    ]
