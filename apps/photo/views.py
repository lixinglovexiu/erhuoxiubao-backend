from common.exceptions import exception_wrapper, ParamsError
from apps.photo import manager as photo_manager


# @auth_required(LoginRole.AUDIT_ADMIN)
@exception_wrapper
def add_get_photos(request):
    if request.method == 'GET':
        return photo_manager.get_photos(request)
    elif request.method == 'POST':
        return photo_manager.add_photos(request)
    else:
        raise ParamsError('错误的request类型：${request.method}')


@exception_wrapper
def get_delete_photo(request, photo_id):
    if request.method == 'GET':
        return photo_manager.get_photo(photo_id)
    elif request.method == 'DELETE':
        return photo_manager.delete_photo(photo_id)
    else:
        raise ParamsError('错误的request类型：${request.method}')


@exception_wrapper
def add_get_photo_walls(request):
    if request.method == 'GET':
        return photo_manager.get_photo_walls()
    elif request.method == 'POST':
        return photo_manager.add_photo_wall(request)
    else:
        raise ParamsError('错误的request类型：${request.method}')


@exception_wrapper
def edit_get_delete_photo_wall(request, photo_wall_id):
    if request.method == 'GET':
        return photo_manager.get_photo_wall(photo_wall_id)
    elif request.method == 'DELETE':
        return photo_manager.delete_photo_wall(photo_wall_id)
    elif request.method == 'PATCH':
        return photo_manager.edit_photo_wall(photo_wall_id, request)
    else:
        raise ParamsError('错误的request类型：${request.method}')


def get_photo_wall_photos(request, photo_wall_id):
    return photo_manager.get_photo_wall_photos(photo_wall_id)