from apps.photo.models import Photo, PhotoWall
from common.exceptions import ParamsError
from common.front_end import request_helper
from common.response import ok
from common.tengxunyun_storage.init import client


def get_photos(request):
    photos = Photo.get_all()
    photos_json = [photo.get_json() for photo in photos]
    return ok({
        'photos': photos_json
    })


def add_photos(request):
    body = request_helper.parse_request_body_to_json(request)
    photos_json = body.get('photos')
    for photo_json in photos_json:
        photo = Photo(
            url=photo_json.get('url'),
            desc=photo_json.get('desc')
        )
        photo.save()
    return ok()


def get_photo(photo_id):
    photo = Photo.get_by_id(photo_id);
    if not photo:
        raise ParamsError('找不到对应的图片')
    return ok({
        'photo': photo.get_json()
    })


def delete_photo(photo_id):
    photo = Photo.get_by_id(photo_id)
    photo.soft_delete()
    return ok()


def get_photo_walls():
    photo_walls = PhotoWall.get_all()
    photo_walls_json = [photo_wall.get_json() for photo_wall in photo_walls]
    return ok({
        'photo_walls': photo_walls_json
    })


def add_photo_wall(request):
    body = request_helper.parse_request_body_to_json(request)

    photo_ids = []
    photos_json = body.get('photos')
    for photo_json in photos_json:
        photo = Photo(
            url=photo_json.get('url'),
            desc=photo_json.get('desc')
        )
        photo.save()
        photo_ids.append(photo.id)

    name = body.get('name')
    photo_wall_type = body.get('type')
    photo_wall_desc = body.get('desc')
    photo_wall = PhotoWall(
        name=name,
        desc=photo_wall_desc,
        photo_ids=photo_ids,
        type=photo_wall_type
    )
    photo_wall.save()

    return ok()


def get_photo_wall(photo_wall_id):
    photo_wall = PhotoWall.get_by_id(photo_wall_id)
    return ok({
        'photo_wall': photo_wall.get_json()
    })


def delete_photo_wall(photo_wall_id):
    photo_wall = PhotoWall.get_by_id(photo_wall_id)
    photo_ids = eval(photo_wall.photo_ids)
    photo_keys = []
    for photo_id in photo_ids:
        photo = Photo.get_by_id(photo_id)
        photo_key = get_photo_key(photo.url)
        photo_keys.append(photo_key)
        if photo:
            photo.delete()

    # 删除存储桶中的文件
    delete_files_from_tengxunyun(photo_keys)

    photo_wall.delete()

    return ok()


def get_photo_key(url: str):
    bucket = 'lixing-1257240793'
    base_url = 'https://' + bucket + '.cos.ap-beijing.myqcloud.com/'
    key = url.replace(base_url, '')
    return key


def delete_files_from_tengxunyun(photo_keys):
    if not photo_keys:
        return
    photo_objects = []
    for photo_key in photo_keys:
        photo_object = {'Key': photo_key}
        photo_objects.append(photo_object)

    client.delete_objects(
        Bucket='lixing-1257240793',
        Delete={
            'Object': photo_objects,
            'Quiet': 'false'
        }
    )


def get_photo_wall_photos(photo_wall_id):
    photo_wall = PhotoWall.get_by_id(photo_wall_id)
    photo_ids = eval(photo_wall.photo_ids)

    photos_json = []
    for photo_id in photo_ids:
        photo = Photo.get_by_id(photo_id)
        photos_json.append(photo.get_json())

    return ok({
        'photos': photos_json
    })


def edit_photo_wall(photo_wall_id, request):
    # 删除之前照片墙上的照片
    photo_wall = PhotoWall.get_by_id(photo_wall_id)
    photo_ids = eval(photo_wall.photo_ids)
    for photo_id in photo_ids:
        photo = Photo.get_by_id(photo_id)
        if photo:
            photo.delete()

    # 保存新的照片
    body = request_helper.parse_request_body_to_json(request)
    photo_ids = []
    photos_json = body.get('photos')
    for photo_json in photos_json:
        photo = Photo(
            url=photo_json.get('url'),
            desc=photo_json.get('desc')
        )
        photo.save()
        photo_ids.append(photo.id)

    # 更新照片墙
    name = body.get('name')
    photo_wall_type = body.get('type')
    photo_wall_desc = body.get('desc')

    photo_wall.name = name
    photo_wall.desc = photo_wall_desc
    photo_wall.photo_ids = photo_ids
    photo_wall.type = photo_wall_type

    photo_wall.save()

    return ok()
