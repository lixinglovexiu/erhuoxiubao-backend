from django.urls import path, include
from apps.photo import views as photo_views

urlpatterns = [
    path('api/v1/photo_wall', include([
        path('', photo_views.add_get_photo_walls),
        path('/<int:photo_wall_id>', photo_views.edit_get_delete_photo_wall),
        path('/<int:photo_wall_id>/photos', photo_views.get_photo_wall_photos),
    ])),
    path('api/v1/photo', include([
        path('', photo_views.add_get_photos),
        path('/<int:photo_id>', photo_views.get_delete_photo),
    ])),
]
